#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

class MatriuSparse
{
    private:
        int files; 
        int columnes;

        std::vector<int> ia;
        std::vector<int> ja;
        std::vector<float> a;
        
        void resize(int files, int columnes) {
            // Las matrices tienen que ser cuadradas siempre por alguna razón...
            int max = std::max(files, columnes);
            this->files = max;
            this->columnes = max;

            if (ia.size())
                ia.resize(files + 1, ia.back());
            else
                ia.resize(files + 1, 0);
        }

    public:
        MatriuSparse();
        MatriuSparse(int files, int columnes);
        MatriuSparse(const MatriuSparse& ms);
        MatriuSparse(const std::string& f);
        ~MatriuSparse();

        void init(int files, int columnes);
        int getNFiles() const;
        int getNColumnes() const;
        bool getVal(int fila, int columna, float& valor) const;
        void setVal(int fila, int columna, float valor);

        MatriuSparse& operator =(const MatriuSparse& ms);
        MatriuSparse operator *(float k) const;
        std::vector<float> operator *(const std::vector<float>& v) const;

        MatriuSparse operator /(float k) const;
        friend std::ostream& operator <<(std::ostream& out, const MatriuSparse& ms);
};
