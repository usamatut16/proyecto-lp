#include <numeric>
#include "MatriuSparse.h"

void MatriuSparse::clear()
{
    a.clear();
    ia.clear();
    ja.clear();
}

int MatriuSparse::getNValues() const { return a.size(); }

void MatriuSparse::calculaGrau(std::vector<int>& graus) const 
{
    for (int i = 1; i < ia.size(); i++)
        graus.push_back(ia[i] - ia[i-1])
}

/*
 * Calcula el vector deltaQ inicial, que es un vector de mapas, uno por cada
 * nodo, donde estos contienen un par como llave, que representa el par de nodos
 * o comunidades que se relacionan, y su deltaQ como valor asociado.
 */
void MatriuSparse::creaMaps(std::vector<std::map<pair<int, int>, double>>& vMaps) const
{
    for (int i = 0; i < ia.size()-1; i++) {
        std::map<pair<int, int>, double> m;
        int l = ia[i];
        int r = ia[i+1];

        while (l < r) {
            m.emplace(std::make_pair(i, ja[l]), 0);
            l++;
        }

        vMaps.push_back(m);
    }
}

void MatriuSparse::calculaDendograms(std::vector<Tree<double>*>& vDendograms) const
{
    for (float f : a)
        vDendograms.push_back(new Tree<double>(f));
}

MatriuSparse::MatriuSparse() { MatriuSparse(0, 0); }

MatriuSparse::MatriuSparse(int files, int columnes) { init(files, columnes); }

MatriuSparse::MatriuSparse(const MatriuSparse& ms) { *this = ms; }

MatriuSparse::MatriuSparse(const std::string& f)
{
    /* 
     * Al suponer que el archivo está ordenado podemos realizar las inserciones
     * "manualmente" sin usar setVal(), esto es en comparación entorno un 20%
     * más rápido con el archivo EpinionsOrdenat.txt
     */
    std::ifstream fitxer(f);
    int fila, columna, maxCol = 0;
    init(0, 0);
    while (fitxer >> fila >> columna) {
        a.push_back(1);
        ja.push_back(columna);
        maxCol = (columna > maxCol) ? columna : maxCol;
        if (fila+1 >= ia.size())
            resize(fila+1, maxCol+1);
        ia.back()+=1;
    }
}

MatriuSparse::~MatriuSparse() = default;

void MatriuSparse::init(int files, int columnes)
{
    if ((files < 1) || (columnes < 1))
        resize(0, 0);

    resize(files, columnes);
}

int MatriuSparse::getNFiles() const { return files; }

int MatriuSparse::getNColumnes() const { return columnes; }

bool MatriuSparse::getVal(int fila, int columna, float& valor) const
{
    if (fila < 0
        || fila > files-1
        || columna < 0
        || columna > columnes-1) return false;

    /* 
     * Iteramos y aumentamos el indice i hasta que se llegue al indice maximo de
     * esa fila o la columna actual ja[i] llegue a ser igual a la que buscamos 
     * (queriendo decir que hay un valor en esa posición ≠ 0) o sea más grande y
     * la columna actual se haya pasado de la que la que buscamos (significando
     * que no existe valor en la columna pedida de la fila)
     */
    int i = ia[fila];
    int max = ia[fila+1];
    while (i < max && ja[i] < columna) i++;

    // Si no existen valores en la fila (i == max) o no hay valores en la fila
    // en esa columna (ja[i] > columna), ya que hemos pasado por todas las
    if (i == max || ja[i] > columna)
        valor = 0;
    else
        valor = a[i];

    return true;
}

void MatriuSparse::setVal(int fila, int columna, float valor)
{
    if (fila < 0 || columna < 0) 
        throw "El valor de fila o columna está fuera del rango de la matriz";

    // Con !valor se evita que se asignen valores =0. Podría quererse reasignar
    // una posición de la matriz a 0 y por tanto borrarla pero es lo que se pide
    if (!valor)
        throw "No se puede insertar un 0";

    else if (fila > files-1 || columna > columnes-1)
        resize(std::max(files, fila + 1), 
               std::max(columnes, columna + 1));

    int i = ia[fila];
    int max = ia[fila+1];
    while (i < max && ja[i] < columna) i++;
    
    if (i == max || ja[i] > columna) { // No hay un valor asignado
        a.insert(a.begin() + i, valor);
        ja.insert(ja.begin() + i, columna);
        std::transform(ia.cbegin() + fila + 1, 
                       ia.cend(),
                       ia.begin() + fila + 1,
                       [](float n) { return n + 1; });
    } else a[i] = valor; // Hay un valor asignado
}

MatriuSparse& MatriuSparse::operator =(const MatriuSparse& ms)
{
    ia = ms.ia;
    ja = ms.ja;
    a = ms.a;
    files = ms.files;
    columnes = ms.columnes;
    return *this;
}

MatriuSparse MatriuSparse::operator *(float k) const
{   
    MatriuSparse ms(*this);
    std::for_each(ms.a.begin(), ms.a.end(), [&](float& n) { n *= k; });
    return ms;
}

MatriuSparse MatriuSparse::operator /(float n) const { return (*this) * (1/n); }

std::vector<float> MatriuSparse::operator *(const std::vector<float>& v) const
{
    if (v.size() != files)
        throw "El número de filas de la matriz tiene que ser igual al \
               número de componentes del vector";

    int i = 0;
    std::vector<float> u;
    std::vector<float>::const_iterator it = a.cbegin();
    while (++i < ia.size())
        u.push_back(v[i-1] * std::accumulate(it + ia[i-1], it + ia[i], 0));
        
    return u;
}

template <typename T>
void printVector(std::ostream& out, std::vector<T> v)
{
    out << "(";
    for (T n : v)
        out << n << "  ";
    out << ")" << std::endl;
}

void printMatrix(const MatriuSparse& ms) {
    float a;
    for (int i=0; i<ms.getNFiles(); i++) {
        for (int j=0; j<ms.getNColumnes(); j++) {
            ms.getVal(i, j, a);
            std::cout << a << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


std::ostream& operator <<(std::ostream& out, const MatriuSparse& ms)
{
    out << "MATRIU DE FILES: " << ms.files << " : COLUMNES: " << ms.columnes \
        << std::endl;

    for (int i=0; i<ms.ia.size()-1; i++) {
        int l = ms.ia[i];
        int r = ms.ia[i+1];
        if (l != r) {
            out << "VALORS FILA:" << i << "(COL:VALOR)" << std::endl;
            while (l < r) {
                out << "(" << ms.ja[l] << " : " << ms.a[l] << ") ";
                l++;
            }
            out << std::endl;
        }
    }

    out << "MATRIUS" << std::endl;
    out << "VALORS" << std::endl;
    printVector(out, ms.a);
    out << "COLS" << std::endl;
    printVector(out, ms.ja);
    out << "INIFILA" << std::endl;
    out << "(";
    for (int i=0; i<ms.ia.size()-1; i++)
        if (ms.ia[i] != ms.ia[i+1])
            out << "[ " << i << " : " << ms.ia[i] << " ] ";

    out << " [Num Elems:" << ms.ia.back() << "] )" << std::endl;

    return out;
}
