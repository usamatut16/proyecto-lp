#include "Comunitat.h"

Comunitat::Comunitat(MatriuSparse* pMAdj)
{
	m_primComdeltaQ = -1;
	m_Q = 0;
	m_pMAdj = pMAdj;
}

Comunitat::~Comunitat()
{
	m_pMAdj = nullptr;	
}
void Comunitat::clear()
{
	m_pMAdj = nullptr;
	m_deltaQ.clear();
	m_indexComs.clear();
	m_maxDeltaQFil.clear();
	m_primComdeltaQ=-1;
	m_vDendrograms.clear();
	m_k.clear();
	m_A.clear();
	m_hTotal.clear();
	m_Q=0;
	m_M2=0;	
}

void Comunitat::calculaA()
{
	
}
void Comunitat::creaIndexComs()
{
	
}
void Comunitat::creaDeltaQHeap()
{
		
}

void Comunitat::modificaVei(int com1, int com2, int vei, int cas)
{

}

void Comunitat::fusiona(int i, int j)
{
    // Declarem vectors pels veins de i (vi), j (vj) i comuns (vij)
    std::vector<int> vi, vj, vij;
    
    for (std::pair<std::pair<int, int>, double> p : m_deltaQ[i])
        vi.push_back(p.first.second);

    for (std::pair<std::pair<int, int>, double> p : m_deltaQ[j])
        vj.push_back(p.first.second);
    
    // Calculem els veins en comú i treiem aquests de vi i vj
    for (std::vector<int>::iterator iti = vi.begin(); iti != vi.end(); ++iti)
        //for (std::vector<int>::iterator itj = vj.begin(); itj != vj.end(); ++itj)
        std::vector<int>::iterator itj = vj.begin();
        while (*iti > *itj) {
            //if (*iti > *itj)
            //    break;
            if (*iti == *itj) {
                vij.push_back(*iti);
                vi.erase(iti);
                vj.erase(itj);
            }

            itj++;
        }

    // Realitzem els cálculs per:
    // els veins en comú
    for (int vei : vij) {
        // Agafem el deltaM de (vei, i) i el sumem al deltaM de (vei, j)
        m_deltaQ[vei][std::make_pair(vei, j)].second 
            += m_deltaQ[vei][std::make_pair(vei, i)].second;

        // Esborrem la relació de (vei, i) 
        m_deltaQ[vei].erase(m_deltaQ[vei].find(std::make_pair(vei, i)));

        // Sumem tambè al deltaM de (j, vei) el deltaM de (i, vei)
        m_deltaQ[j][std::make_pair(j, vei)].second 
            += m_deltaQ[i][std::make_pair(i, vei)].second;
    }

    // els veins només de i
    for (int vei : vi) {
        // Agafem el deltaM de (vei, i)
        it = m_deltaQ[vei].find(std::make_pair(vei, i));
        deltaMvi = it->second;

        // Calculem un deltaM per (vei, j)
        double t = 2 * m_A[j] * m_A[vei]
        m_deltaQ[vei].emplace(std::make_pair(vei, j), deltaMvi - t);

        // Esborrem la relacio de (vei, i)
        m_deltaQ[vei].erase(it);

        // Calculem un deltaM per (j, vei)
        deltaMjv = m_deltaQ[j][std::make_pair(i, vei)].second - t;
        m_deltaQ[j].emplace(std::make_pair(j, vei), deltaMjv);
    }
        
    // els veins només de j
    for (int vei : vj) {
        double t = 2 * m_A[i] * m_A[vei]
        m_deltaQ[vei][std::make_pair(vei, j)].second -= t;
        m_deltaQ[j][std::make_pair(j, vei)].second -= t;
    }

    // TODO modificar m_maxDeltaQFil i hTotal
    
    // Eliminar els elements del mapa m_deltaQ[i]
    m_deltaQ[i].clear();

    // Actualizem el vector de comunitats actives i la primera comunitat activa
    // si escau (si borrem aquesta amb la fusió)
    if (i == m_primComdeltaQ)
        m_primComdeltaQ = m_indexComs[m_indexComs[i].second];
    else
        m_indexComs[m_indexComs[i].first].second = m_indexComs[i].second;

    if (i != m_primComdeltaQ.size())
        m_indexComs[m_indexComs[i].second].first = m_indexComs[i].first;

    // TODO Fusionem el arbres i-j del vector que representa el dendograma

    // Afegim el grau normalitzat de la comunitat i a la j
    m_A[j] += m_A[i];
}

void Comunitat::generaDendrogram(int pos1, int pos2, double deltaQp1p2)
{
}

void Comunitat::calculaComunitats(list<Tree<double>*>& listDendrogram)
{

}

